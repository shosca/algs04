public class WeightedQuickUnion {
    public int[] id;
    public int[] sz;
    private int count;

    public WeightedQuickUnion(int N) {
        id = new int[N];
        sz = new int[N];
        count = N;
        for (int i = 0; i < N; i++) {
            id[i] = i;
            sz[i] = 1;
        }

    }

    public int find(int p) {
        while (p != id[p])
            p = id[p];

        return p;
    }

    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    public void union(int p, int q) {
        int i = find(p);
        int j = find(q);
        if (i == j) return;

        if (sz[i] < sz[j]) {
            id[i] = j;
            sz[j] += sz[i];
        } else {
            id[j] = i;
            sz[i] += sz[j];
        }
        count -= 1;
    }

    public static void main(String[] args) {
        int N = 10;
        WeightedQuickUnion uf = new WeightedQuickUnion(N);
        uf.union(6,3);
        uf.union(8,7);
        uf.union(9,6);
        uf.union(0,5);
        uf.union(3,1);
        uf.union(9,2);
        uf.union(1,4);
        uf.union(8,0);
        uf.union(6,7);

        for (int i = 0; i < N; i++)
            StdOut.printf("%s ", uf.id[i]);

        StdOut.println();
    }
}
