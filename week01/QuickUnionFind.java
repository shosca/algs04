public class QuickUnionFind {
    public int[] id;
    private int count;

    public QuickUnionFind(int N) {
        id = new int[N];
        count = N;
        for (int i = 0; i < N; i++)
            id[i] = i;
    }

    public int find(int p) {
        while (p != id[p])
            p = id[p];

        return p;
    }

    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    public void union(int p, int q) {
        int i = find(p);
        int j = find(q);
        if (i == j) return;
        id[i] = j;
        count -= 1;
    }

    public static void main(String[] args) {
        int N = 10;
        QuickUnionFind uf = new QuickUnionFind(N);
        uf.union(7, 1);
        uf.union(5, 7);
        uf.union(9, 0);
        uf.union(0, 4);
        uf.union(2, 9);
        uf.union(2, 8);

        for (int i = 0; i < N; i++)
            StdOut.printf("%s ", uf.id[i]);

        StdOut.println();
    }
}
