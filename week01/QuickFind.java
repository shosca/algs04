public class QuickFind {
    public int[] id;
    private int count;

    public QuickFind(int N) {
        count = N;
        id = new int[N];
        count = N;
        for (int i = 0; i < N; i++)
            id[i] = i;
    }

    public int find(int p) {
        return id[p];
    }

    public boolean connected(int p, int q) {
        return id[p] == id[q];
    }

    public void union(int p, int q) {
        if (connected(p, q)) return ;

        int pid = id[p];
        for (int i = 0; i < id.length; i++)
            if (id[i] == pid)
                id[i] = id[q];

        count -= 1;
    }

    public static void main(String[] args) {
        int N = 10;
        QuickFind uf = new QuickFind(N);
        uf.union(0,4);
        uf.union(2,4);
        uf.union(6,4);
        uf.union(4,3);
        uf.union(6,7);
        uf.union(2,9);

        for (int i = 0; i < N; i++)
            StdOut.printf("%s ", uf.id[i]);

        StdOut.println();
    }
}
