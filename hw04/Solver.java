import java.util.HashSet;

public class Solver {
    private SearchState solution;

    public Solver(Board initial) {
        Heuristic h = new ManhattanHeuristic();
        if (initial.isGoal()) {
            solution = new SearchState(initial, h, null);
        } else {
            solution = search(initial, h);
        }
    }

    private SearchState search(Board initial, Heuristic h) {
        MinPQ<SearchState> fringe = new MinPQ<SearchState>();
        MinPQ<SearchState> twinfringe = new MinPQ<SearchState>();

        fringe.insert(new SearchState(initial, h, null));
        twinfringe.insert(new SearchState(initial.twin(), h, null));

        HashSet<SearchState> visited = new HashSet<SearchState>();
        HashSet<SearchState> twinvisited = new HashSet<SearchState>();

        while (!fringe.isEmpty() || !twinfringe.isEmpty()) {
            SearchState s = fringe.delMin();
            SearchState twins = twinfringe.delMin();

            if (s.board.isGoal()) {
                return s;
            }

            if (twins.board.isGoal()) {
                return null;
            }

            doneighbors(s, h, fringe, visited);
            doneighbors(twins, h, twinfringe, twinvisited);
        }
        return null;
    }

    private void doneighbors(SearchState s, Heuristic h,
            MinPQ<SearchState> fringe, HashSet<SearchState> visited) {

        if (!visited.contains(s)) {
            visited.add(s);
            for (Board n: s.board.neighbors()) {
                SearchState nstate = new SearchState(n, h, s);
                fringe.insert(nstate);
            }
        }
    }

    public boolean isSolvable() {
        return solution != null;
    }

    public int moves() {
        if (solution == null) return -1;

        return solution.moves;
    }

    public Iterable<Board> solution() {
        if (solution == null) return null;

        Stack<Board> result = new Stack<Board>();
        for (SearchState s = solution; s != null; s = s.previousState)
            result.push(s.board);

        return result;
    }

    private static int[][] readBlocks(String file) {
        In in = new In(file);
        int size = in.readInt();
        int[][] blocks = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                blocks[i][j] = in.readInt();
            }
        }
        return blocks;
    }

    public static void main(String[] args) {
        Board initial = new Board(readBlocks(args[0]));

        Solver s = new Solver(initial);

        if (!s.isSolvable()) {
            StdOut.println("No solution possible");
        } else {
            StdOut.printf("Minimum number of moves = %s\n", s.moves());
            for (Board b : s.solution()) {
                StdOut.println(b);
            }
        }
    }

    private interface Heuristic {
        int heuristic(Board b);
    }

    private class ManhattanHeuristic implements Heuristic {
        public int heuristic(Board b) {
            return b.manhattan();
        }
    }

    private class HammingHeuristic implements Heuristic {
        public int heuristic(Board b) {
            return b.hamming();
        }
    }

    private class CombinedHeuristic implements Heuristic {
        private Heuristic first;
        private Heuristic second;
        public CombinedHeuristic(Heuristic h1, Heuristic h2) {
            first = h1;
            second = h2;
        }

        public int heuristic(Board b) {
            return first.heuristic(b) + second.heuristic(b);
        }

    }

    private class SearchState implements Comparable<SearchState> {
        private final Board board;
        private final SearchState previousState;
        private final int priority;
        private final int moves;

        public SearchState(Board b, Heuristic h, SearchState prev) {
            board = b;
            previousState = prev;
            if (previousState == null)
                moves = 0;
            else
                moves = previousState.moves + 1;

            priority = h.heuristic(board) + moves;
        }

        public int priority() {
            return priority;
        }

        public int compareTo(SearchState that) {
            return this.priority - that.priority;
        }

        public int hashCode() {
            return this.board.toString().hashCode();
        }

        public boolean equals(Object other) {
            if (other == this) return true;
            if (other == null) return false;
            if (other.getClass() != this.getClass()) return false;

            SearchState that = (SearchState) other;
            return this.board.equals(that.board);
        }
    }
}
