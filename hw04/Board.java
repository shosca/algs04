import java.util.ArrayList;
import java.util.Random;

public class Board {
    private int size;
    private int[][] blocks;
    private int row;
    private int col;
    private int m = -1;

    public Board(int[][] b) {
        this(b, true);
    }

    private Board(int[][] b, boolean docopy) {
        size = b.length;
        if (docopy)
            this.blocks = copy(b);
        else
            this.blocks = b;

        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                if (blocks[r][c] == 0) {
                    this.row = r;
                    this.col = c;
                    break;
                }
            }
        }
    }

    public Iterable<Board> neighbors() {
        ArrayList<Board> states = new ArrayList<Board>();
        if (row > 0)
            states.add(clone(row, col, row - 1, col));

        if (row < size - 1)
            states.add(clone(row, col, row + 1, col));

        if (col > 0)
            states.add(clone(row, col, row, col - 1));

        if (col < size - 1)
            states.add(clone(row, col, row, col + 1));

        return states;
    }

    public Board twin() {
        int[][] copy = copy(blocks);
        Random random = new Random();

        int r = random.nextInt(size);
        while (r == row)
            r = random.nextInt(size);

        int c = random.nextInt(size - 1) + 1;

        //StdOut.printf("swap %s: %s <-> %s \n", r, c, c - 1);

        copy[r][c] = blocks[r][c - 1];
        copy[r][c - 1] = blocks[r][c];

        return new Board(copy, false);
    }

    private Board clone(int fromrow, int fromcol, int torow, int tocol) {
        int[][] copy = copy(blocks);
        int tmp = copy[torow][tocol];
        copy[torow][tocol] = copy[fromrow][fromcol];
        copy[fromrow][fromcol] = tmp;
        return new Board(copy, false);
    }

    private int[][] copy(int[][] b) {
        int[][] tmp = new int[b.length][b.length];
        for (int r = 0; r < b.length; r++) {
            for (int c = 0; c < b.length; c++) {
                tmp[r][c] = b[r][c];
            }
        }
        return tmp;
    }

    public int dimension() {
        return size;
    }

    public int manhattan() {
        if (m > -1) return m;

        m = 0;
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {

                int val = blocks[r][c];
                if (val < 1) continue;

                int er = (val - 1) / size;
                int ec = (val - 1) % size;
                m += Math.abs(r - er) + Math.abs(c - ec);
            }
        }
        return m;
    }

    public int hamming() {
        int count = 0, e = 1;
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                if (blocks[r][c] != e++)
                    count += 1;
            }
        }
        return count - 1;
    }

    public boolean isGoal() {
        int e = 0;
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                e += 1;

                if (r == size - 1 && c == size - 1) continue;

                if (e != blocks[r][c])
                    return false;
            }
        }
        return true;

    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(size);
        s.append("\n");
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                s.append(String.format("%2d ", blocks[r][c]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;

        Board that = (Board) other;

        if (this.size != that.size) return false;

        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                if (this.blocks[r][c] != that.blocks[r][c])
                    return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] blocks = readBlocks(args[0]);

        Board initial = new Board(blocks);

        StdOut.println(initial.toString());
        StdOut.printf("Hamming: %s\n", initial.hamming());
        StdOut.printf("Manhattan: %s\n", initial.manhattan());
        StdOut.printf("IsGoal: %s\n", initial.isGoal());

        StdOut.printf("Twin: %s\n", initial.twin());

        //StdOut.println("Neighbors:");
        //for (Board n: initial.neighbors())
            //StdOut.println(n.toString());

        //if (args.length > 1) {
            //StdOut.println(initial.equals(new Board(readBlocks(args[1]))));
        //}
    }

    private static int[][] readBlocks(String file) {
        In in = new In(file);
        int size = in.readInt();
        int[][] blocks = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                blocks[i][j] = in.readInt();
            }
        }
        return blocks;
    }
}
