import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] items;
    private int N;

    public RandomizedQueue() {
        items = (Item[]) new Object[2];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public int size() {
        return N;
    }

    public void enqueue(Item item) {
        if (item == null)
            throw new NullPointerException();

        if (N == items.length) {
            resize(N * 2);
        }

        items[N++] = item;
    }

    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException();

        int i = random(N);
        Item item = items[i];
        items[i] = items[N-1];
        items[N-1] = null;
        N--;

        if (N > 0 && N == items.length / 4) {
            resize(items.length / 2);
        }

        return item;
    }

    public Item sample() {
        if (isEmpty())
            throw new NoSuchElementException();

        return items[random(N)];
    }

    private void resize(int c) {
        Item[] a = (Item[]) new Object[c];
        for (int i = 0; i < N; i++)
            a[i] = items[i];

        items = a;
    }

    private static int random(int n) {
        return StdRandom.uniform(n);
    }

    public Iterator<Item> iterator() {
        return new RandomIterator(items, N);
    }

    private class RandomIterator implements Iterator<Item> {
        private Item[] items;
        private int idx;

        public RandomIterator(Item[] items, int N) {
            idx = N;
            this.items = (Item[]) new Object[idx];
            for (int i = 0; i < idx; i++) {
                this.items[i] = items[i];
            }
        }

        public boolean hasNext() {
            return idx > 0;
        }

        public Item next() {
            if (!hasNext())
                throw new NoSuchElementException();

            int i = random(idx);
            Item item = items[i];
            items[i] = items[idx - 1];
            items[idx - 1] = null;
            idx--;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
