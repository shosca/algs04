import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private class Node {
        private Item item;
        private Node next;
        private Node prev;

        public Node() { }
        public Node(Item item, Node prev, Node next) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }

    private Node head;
    private Node tail;
    private int size;

    public Deque() {
        this.size = 0;
        head = new Node();
        tail = new Node();
        head.next = tail;
        tail.prev = head;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        if (item == null)
            throw new NullPointerException();

        Node n = new Node(item, head, head.next);
        head.next.prev = n;
        head.next = n;
        size++;
    }

    public void addLast(Item item) {
        if (item == null)
            throw new NullPointerException();

        Node n = new Node(item, tail.prev, tail);
        tail.prev.next = n;
        tail.prev = n;
        size++;
    }

    public Item removeFirst() {
        if (isEmpty())
            throw new NoSuchElementException();

        Node n = head.next;
        n.next.prev = head;
        head.next = n.next;
        n.prev = null;
        n.next = null;
        size--;
        return n.item;
    }

    public Item removeLast() {
        if (isEmpty())
            throw new NoSuchElementException();

        Node n = tail.prev;
        n.prev.next = tail;
        tail.prev = n.prev;
        n.prev = null;
        n.next = null;
        size--;
        return n.item;

    }

    public Iterator<Item> iterator() {
        return new QIterator();
    }

    private class QIterator implements Iterator<Item> {
        private Node current = head.next;

        public boolean hasNext() {
            return current != tail;
        }

        public Item next() {
            if (!hasNext())
                throw new NoSuchElementException();

            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
