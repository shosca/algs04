
public class PercolationStats {
    private double[] results;
    private int n;

    public PercolationStats(int N, int T) {
        if (T < 1 || N < 1)
            throw new IllegalArgumentException("T or N cannot be zero");

        n = N;
        results = new double[T];

        double counter = 0;

        for (int i = 0; i < T; i++) {
            results[i] = (double) runonce() / (n*n);
        }
    }

    private int runonce() {
        int counter = 0;
        Percolation p = new Percolation(n);
        while (!p.percolates()) {
            int x = StdRandom.uniform(n) + 1;
            int y = StdRandom.uniform(n) + 1;
            if (!p.isOpen(x, y)) {
                p.open(x, y);
                counter++;
            }
        }
        return counter;
    }

    public double mean() {
        return StdStats.mean(results);
    }

    public double stddev() {
        return StdStats.stddev(results);
    }

    public double confidenceHi() {
        return ci(1);
    }

    public double confidenceLo() {
        return ci(-1);
    }

    private double ci(int side) {
        return mean() + side * (1.96 * stddev()) / Math.sqrt(results.length);
    }

    public static void main(String[] args) {
        int N, T;
        PercolationStats p;
        N = Integer.parseInt(args[0]);
        T = Integer.parseInt(args[1]);
        System.out.println(N + " " + T);
        p = new PercolationStats(N, T);
        System.out.println("mean                    = " + p.mean());
        System.out.println("stddev                  = " + p.stddev());
        System.out.println("95% confidence interval = "
                + (p.confidenceLo() - p.confidenceHi()));
    }
}
