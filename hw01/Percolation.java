
public class Percolation {

    private int n;
    private int top = 0;
    private int bottom = 0;
    private WeightedQuickUnionUF uf;
    private WeightedQuickUnionUF ufnb;
    private boolean[] states;

    public Percolation(int N) {
        n = N;
        top = 0;
        bottom = n*n+1;
        states = new boolean[n*n+2];
        states[top] = true;
        states[bottom] = true;
        uf = new WeightedQuickUnionUF(n*n+2);
        ufnb = new WeightedQuickUnionUF(n*n+1);

        for (int i = 1; i <= n; i++) {
            uf.union(top, xyto1d(1, i));
            ufnb.union(top, xyto1d(1, i));
        }

        for (int i = 1; i <= n; i++) {
            uf.union(bottom, xyto1d(n, i));
        }
    }

    public void open(int i, int j) {
        if (isOpen(i, j)) return;

        states[xyto1d(i, j)] = true;

        // top
        if (i > 1 && isOpen(i-1, j)) union(i, j, i-1, j);

        // bottom
        if (i < n && isOpen(i+1, j)) union(i, j, i+1, j);

        // left
        if (j > 1 && isOpen(i, j-1)) union(i, j, i, j-1);

        // right
        if (j < n && isOpen(i, j+1)) union(i, j, i, j+1);
    }

    public boolean isOpen(int i, int j) {
        return states[xyto1d(i, j)];
    }

    public boolean isFull(int i, int j) {
        return isOpen(i, j) && ufnb.connected(top, xyto1d(i, j));
    }

    public boolean percolates() {
        if (n < 2)
            return isOpen(1, 1) && uf.connected(top, bottom);
        else
            return uf.connected(top, bottom);
    }

    private int xyto1d(int x, int y) {
        if (x < 1 || x > n || y < 1 || y > n)
            throw new IndexOutOfBoundsException(x
                    + " and " + y + " was out of bounds");

        return n*(x-1)+y;
    }

    private void union(int i1, int j1, int i2, int j2) {
        uf.union(xyto1d(i1, j1), xyto1d(i2, j2));
        ufnb.union(xyto1d(i1, j1), xyto1d(i2, j2));
    }

    public static void main(String[] args) {
        Percolation p = new Percolation(1);
        System.out.println(p.isOpen(1, 1));
        System.out.println(p.percolates());
    }

}
