/*************************************************************************
 * Name:
 * Email:

 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Comparator;

public class Point implements Comparable<Point> {

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new SlopeComparator();

    private class SlopeComparator implements Comparator<Point> {
        public int compare(Point p1, Point p2) {
            return Double.compare(slopeTo(p1), slopeTo(p2));
        }
    }

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        if (this.compareTo(that) == 0)
            return Double.NEGATIVE_INFINITY;

        double dx = that.x - this.x;
        double dy = that.y - this.y;

        if (dy == 0.0)
            return 0.0;

        if (dx == 0.0)
            return Double.POSITIVE_INFINITY;

        return dy/dx;
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    public int compareTo(Point that) {
        if (this.y == that.y && this.x == that.x)
            return 0;

        if (this.y < that.y || (this.y == that.y && this.x < that.x))
            return -1;

        return 1;
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    // unit test
    public static void main(String[] args) {
        Point p1, p2;

        p1 = new Point(0, 10);
        p2 = new Point(0, 5);
        if (p1.slopeTo(p2) == Double.POSITIVE_INFINITY)
            System.out.println("Pass: Vertical line should have Infinity slope");
        else
            System.out.println("Fail: Vertical line should have Infinity slope");

        p1 = new Point(10, 0);
        p2 = new Point(5, 0);
        if (p1.slopeTo(p2) == 0.0)
            System.out.println("Pass: Horizontal line should have 0 slope");
        else
            System.out.println("Fail: Horizontal line should have 0 slope");

        if (p1.slopeTo(p1) == Double.NEGATIVE_INFINITY)
            System.out.println("Pass: Slope of a point with itself is -Infinity");
        else
            System.out.println("Fail: Slope of a point with itself is -Infinity");

        p1 = new Point(2, 2);
        p2 = new Point(10, 10);
        String s = "Slope of " + p1.toString() + " and "
            + p2.toString() + " should be 1";
        if (p1.slopeTo(p2) == 1)
            System.out.println("Pass: " + s);
        else
            System.out.println("Fail: " + s);

        p1 = new Point(0, 0);
        p2 = new Point(10, 20);
        s = "Slope of " + p1.toString() + " and " + p2.toString() + " should be 2";
        if (p1.slopeTo(p2) == 2)
            System.out.println("Pass: " + s);
        else
            System.out.println("Fail: " + s);

        p1 = new Point(10, 20);
        p2 = new Point(0, 0);
        s = "Slope of " + p1.toString() + " and " + p2.toString() + " should be 2";
        if (p1.slopeTo(p2) == 2)
            System.out.println("Pass: " + s);
        else
            System.out.println("Fail: " + s);

        p1 = new Point(-10, 20);
        p2 = new Point(0, 0);
        s = "Slope of " + p1.toString() + " and " + p2.toString() + " should be -2";
        if (p1.slopeTo(p2) == -2)
            System.out.println("Pass: " + s);
        else
            System.out.println("Fail: " + s);
    }
}
