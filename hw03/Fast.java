import java.util.Arrays;

public class Fast {
    public static void main(String[] args) {
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);

        Point[] points = readPoints(args[0]);

        Arrays.sort(points);

        findcoll(points);

        StdDraw.show(0);
    }

    private static void findcoll(Point[] points) {
        int N = points.length;

        Arrays.sort(points);
        Point l = null;

        for (int i = 0; i < N; ++i) {
            Point p = points[i];
            p.draw();

            Point[] sorted = new Point[N];
            System.arraycopy(points, 0, sorted, 0, sorted.length);
            Arrays.sort(sorted, i, N, p.SLOPE_ORDER);

            double last = p.slopeTo(p);
            int lo = 0, hi = 0;

            for (int j = i + 1; j < N; j++) {
                Point q = sorted[j];

                double curr = p.slopeTo(q);

                if (curr == last) {
                    hi += 1;
                } else {
                    if (hi - lo >= 2 && sorted[hi] != l) {
                        output(p, sorted, lo, hi);
                        l = sorted[hi];
                    }
                    lo = j;
                    hi = j;
                    last = curr;
                }
            }

            if (hi - lo >= 2 && sorted[hi] != l) {
                output(p, sorted, lo, hi);
                l = sorted[hi];
            }

        }

    }

    private static void output(Point p, Point[] points, int lo, int hi) {
        StdOut.printf("%s", p);
        for (int k = lo; k <= hi; k++) {
            StdOut.printf(" -> %s", points[k]);
        }
        StdOut.println();
        p.drawTo(points[hi]);
    }

    private static Point[] readPoints(String file) {
        In in = new In(file);
        Point[] points = new Point[in.readInt()];
        for (int i = 0; i < points.length; i++) {
            points[i] = new Point(in.readInt(), in.readInt());
        }
        return points;
    }
}
