import java.util.Arrays;

public class Brute {
    public static void main(String[] args) {
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);

        Point[] points = readPoints(args[0]);
        Arrays.sort(points);
        int n = points.length;

        for (int i = 0; i < n; i++) {
            Point p = points[i];
            p.draw();

            for (int j = i + 1; j < n; j++) {
                Point q = points[j];
                double pq = p.slopeTo(q);

                for (int k = j + 1; k < n; k++) {
                    Point r = points[k];

                    if (pq != p.slopeTo(r)) continue;

                    for (int l = k + 1; l < n; l++) {

                        Point s = points[l];

                        if (pq != p.slopeTo(s)) continue;

                        p.drawTo(s);
                        StdOut.printf("%s -> %s -> %s -> %s\n", p, q, r, s);
                    }
                }
            }
        }

        StdDraw.show(0);
    }

    private static Point[] readPoints(String file) {
        In in = new In(file);
        Point[] points = new Point[in.readInt()];
        for (int i = 0; i < points.length; i++) {
            points[i] = new Point(in.readInt(), in.readInt());
        }
        return points;
    }
}
