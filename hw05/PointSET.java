public class PointSET {
    private final SET<Point2D> set = new SET<Point2D>();

    public boolean isEmpty() {
        return set.size() == 0;
    }

    public int size() {
        return set.size();
    }

    public void insert(Point2D p) {
        set.add(p);
    }

    public boolean contains(Point2D p) {
        return set.contains(p);
    }

    public void draw() {
        for (Point2D p: set) {
            p.draw();
        }
    }

    public Iterable<Point2D> range(RectHV rect) {
        Stack<Point2D> s = new Stack<Point2D>();

        for (Point2D p: set) {
            if (rect.contains(p))
                s.push(p);
        }
        return s;
    }

    public Point2D nearest(Point2D p) {
        if (set.size() == 0) return null;

        Point2D n = null;
        for (Point2D q: set) {
            if (n == null) n = q;

            if (q.distanceSquaredTo(p) < n.distanceSquaredTo(p)) {
                n = q;
            }
        }

        return n;
    }

}
