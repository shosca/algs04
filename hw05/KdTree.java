import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;

public class KdTree {

    private class Node
    {
        private Node left;
        private Node right;
        private Point2D point;

        public Node(Point2D p)
        {
            point = p;
        }
    }

    private Node root = null;
    private int n;

    public boolean isEmpty()
    {
        return root == null;
    }

    public int size()
    {
        return n;
    }

    public void insert(Point2D p)
    {
        root = insert(root, p, true);
    }

    private Node insert(Node node, Point2D p, boolean type)
    {
        if (node == null) {
            n += 1;
            return new Node(p);
        }

        if (node.point.equals(p))
            return node;

        if (getCmp(type).compare(p, node.point) < 0)
            node.left = insert(node.left, p, !type);
        else
            node.right = insert(node.right, p, !type);

        return node;
    }

    private Comparator<Point2D> getCmp(boolean type)
    {
        Comparator<Point2D> cmp = Point2D.Y_ORDER;
        if (type)
            cmp = Point2D.X_ORDER;

        return cmp;
    }

    public boolean contains(Point2D p)
    {
        if (p == null || isEmpty())
            return false;

        return contains(root, p, true);
    }

    private boolean contains(Node node, Point2D p, boolean type)
    {
        if (node == null) return false;

        if (node.point.equals(p))
            return true;

        if (getCmp(type).compare(p, node.point) < 0)
            return contains(node.left, p, !type);
        else
            return contains(node.right, p, !type);
    }

    public void draw()
    {
        draw(root);

        StdDraw.show(0);
    }

    private void draw(Node node)
    {
        if (node == null) return;

        node.point.draw();
        draw(node.left);
        draw(node.right);
    }

    public Iterable<Point2D> range(RectHV rect)
    {
        List<Point2D> range = new ArrayList<Point2D>();

        range(root, rect, range, true);

        return range;
    }

    private void range(Node node, RectHV rect, List<Point2D> r, boolean type)
    {
        if (node == null) return;

        if (rect.contains(node.point))
            r.add(node.point);

        double p = node.point.y();
        double min = rect.ymin();
        double max = rect.ymax();
        if (type) {
            p = node.point.x();
            min = rect.xmin();
            max = rect.xmax();
        }

        if (min < p)
            range(node.left, rect, r, !type);
        if (max >= p)
            range(node.right, rect, r, !type);

    }

    public Point2D nearest(final Point2D p)
    {
        Comparator<Point2D> cmp = new Comparator<Point2D>() {
            public int compare(Point2D p1, Point2D p2) {
                double d1 = p.distanceSquaredTo(p1);
                double d2 = p.distanceSquaredTo(p2);
                if (d1 < d2) return -1;
                if (d1 > d2) return +1;
                return 0;
            }
        };

        MinPQ<Point2D> queue = new MinPQ<Point2D>(cmp);
        nearest(root, new RectHV(0.0, 0.0, 1.0, 1.0), p, queue, true);
        return queue.min();
    }

    private void nearest(Node node, RectHV rect, Point2D query,
            MinPQ<Point2D> queue, boolean type)
    {
        if (node == null) return;

        Point2D point = node.point;
        queue.insert(point);

        if (type) {
            Point2D bottom = new Point2D(point.x(), rect.ymin());
            Point2D top = new Point2D(point.x(), rect.ymax());

            RectHV leftrec = new RectHV(rect.xmin(), rect.ymin(),
                    top.x(), top.y());
            RectHV rightrec = new RectHV(bottom.x(), bottom.y(),
                    rect.xmax(), rect.ymax());

            if (query.x() < point.x()) {
                nearestsubtree(node.left, node.right, leftrec, rightrec,
                        query, queue, !type);
            } else {
                nearestsubtree(node.right, node.left, rightrec, leftrec,
                        query, queue, !type);
            }
        } else {
            Point2D left = new Point2D(rect.xmin(), point.y());
            Point2D right = new Point2D(rect.xmax(), point.y());

            RectHV bottomrec = new RectHV(rect.xmin(), rect.ymin(),
                    right.x(), right.y());
            RectHV toprec = new RectHV(left.x(), left.y(),
                    rect.xmax(), rect.ymax());

            if (query.y() < point.y()) {
                nearestsubtree(node.left, node.right, bottomrec, toprec,
                        query, queue, !type);
            } else {
                nearestsubtree(node.right, node.left, toprec, bottomrec,
                        query, queue, !type);
            }
        }
    }

    private void nearestsubtree(Node first, Node second,
            RectHV firstrec, RectHV secondrec,
            Point2D query, MinPQ<Point2D> queue, boolean type)
    {
        nearest(first, firstrec, query, queue, type);

        double dc = queue.min().distanceSquaredTo(query);
        double ds = secondrec.distanceSquaredTo(query);
        if (ds < dc) {
            nearest(second, secondrec, query, queue, type);
        }
    }
}
