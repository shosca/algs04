# Stick this one at the top of your Algorithms class directory; it
# will be sourced from all the individual code directories.

.PHONY: build run checkstyle findbugs deliver clean cleanall

# My java binaries, classpath and class binary path are all already
# sourced in my ~/.bashrc, so I don't need to specify them here:
# JAVAC = "$(JAVA_HOME)/bin/javac"
# JAVA = "$(JAVA_HOME)/bin/java"
# JAR = "$(JAVA_HOME)/bin/jar"

CP = .:$(ALGO)/stdlib.jar:$(ALGO)/algs4.jar

JAVAC = javac -Xlint:unchecked
JAVA = java
JAR = jar

# Change this to your algo class directory:
ALGO = $(HOME)/algs4

# Optional
ALGOBIN = $(ALGO)/bin
ALGODATA = $(ALGO)/data
ALGOSRC = $(ALGO)/src

PROG ?= $(word 1,$(CLASSES))
CLASSFILES = $(patsubst %,%.class,$(CLASSES))
TESTCLASSES = $(patsubst %,Test%,$(CLASSES))
TESTFILES = $(patsubst %,%.class,$(TESTCLASSES))
JAVAFILES = $(patsubst %,%.java,$(CLASSES))

%.class: %.java
	$(JAVAC) -cp $(CP) $<

build: $(CLASSFILES)

run: build
	$(JAVA) -cp $(CP) $(PROG) $(CMDLINE)

checkstyle: $(JAVAFILES)
	$(ALGOBIN)/checkstyle $^

findbugs: $(CLASSFILES)
	$(ALGOBIN)/findbugs $^

test: build $(TESTFILES)
	$(JAVA) org.junit.runner.JUnitCore $(TESTCLASSES)

# By default, 'deliver' runs your unit tests. If you're not unit
# testing, you can replace 'test' with 'build':
# deliver: build checkstyle findbugs
deliver: test checkstyle findbugs
	$(JAR) cfMv $(PROG).zip $(JAVAFILES)

clean:
	rm -f *.class

cleanall: clean
	rm -f *.zip
